#[derive(Clone, Copy)]
struct GraphNode {
    val: usize,
    f: u32,
    g: u32,
    h: u32,
    visited: bool
}

impl GraphNode {
    fn new(val: usize, h: u32, g: u32) -> Self {
        Self {val, h, g, visited: false, f: h+g}
    }
}

struct Graph {
    vertices: usize,
    adjacency_list: Vec<Vec<GraphNode>>,
    tour_list: Vec<Vec<bool>>,
}

impl Graph {
    const sides: [(isize, isize); 4] = [(-1, 0), (0, 1), (1, 0), (0, -1)];
    const diagonal: [(isize, isize); 4] = [(-1, 1), (1, 1), (1, -1), (-1, -1)];

    fn new(n_row: usize, n_col: usize) -> Self {
        Graph {
            vertices: std::cmp::max(n_row, n_col),
            adjacency_list: vec![vec![GraphNode::new(0,0,0);n_col];n_row],
            tour_list: vec![vec![false;n_col];n_row],
        }
    }

    fn add_edge(&mut self, origin: usize, destination: usize, val: usize) {
        self.adjacency_list[origin][destination].val = val;
    }

    fn display(self: &Self) {
        println!();
        for row in &self.adjacency_list {
            for col in row {
                print!("{} ", col.val);
            }
            println!();
        }
        println!();
    }

    fn display_tour(self: &Self) {
        println!();
        for row in &self.tour_list {
            for col in row {
                print!("{:>2} ", if *col {'1'} else {'0'});
            }
            println!();
        }
        println!();
    }

    fn f_calc(self: &mut Self, origin: (usize, usize), destination: (usize, usize)) -> usize {
        return 10 * ( 
            usize::abs_diff(origin.0, destination.0) 
            + usize::abs_diff(origin.1, destination.1) 
        );
    }

    fn get_neighbors(self: & mut Self, cur_pos: (usize, usize), destination: (usize, usize)) 
    -> Vec<(usize, usize)> {
        let cur_row = cur_pos.0 as isize;
        let cur_col = cur_pos.1 as isize;        
        let mut neigbors: Vec<(usize, usize)> = Vec::new();

        for (side_row, side_col) in &Graph::sides {
            let row = cur_row + side_row;
            let col = cur_col + side_col;
            if row >= 0 && row < self.adjacency_list.len() as isize 
            && col >= 0 && col < self.adjacency_list[0].len() as isize
            && self.adjacency_list[row as usize][col as usize].val != 1 
            && !self.adjacency_list[row as usize][col as usize].visited {
                neigbors.push((row as usize, col as usize));
                self.adjacency_list[row as usize][col as usize].g = 10; // g calc
                self.adjacency_list[row as usize][col as usize].h =
                10 * ( 
                    usize::abs_diff(row as usize, destination.0) 
                    + usize::abs_diff(col as usize, destination.1) 
                ) as u32; // h calc
                self.adjacency_list[row as usize][col as usize].f = 
                    self.adjacency_list[row as usize][col as usize].g
                    + self.adjacency_list[row as usize][col as usize].h; // f calc
            }
        }

        for (side_row, side_col) in &Graph::diagonal {
            let row = cur_row + side_row;
            let col = cur_col + side_col;
            if row >= 0 && row < self.adjacency_list.len() as isize 
            && col >= 0 && col < self.adjacency_list[0].len() as isize
            && self.adjacency_list[row as usize][col as usize].val != 1 
            && !self.adjacency_list[row as usize][col as usize].visited {
                neigbors.push((row as usize, col as usize));
                self.adjacency_list[row as usize][col as usize].g = 14; // g calc
                self.adjacency_list[row as usize][col as usize].h =
                10 * ( 
                    usize::abs_diff(row as usize, destination.0) 
                    + usize::abs_diff(col as usize, destination.1) 
                ) as u32; // h calc
                self.adjacency_list[row as usize][col as usize].f = 
                    self.adjacency_list[row as usize][col as usize].g
                    + self.adjacency_list[row as usize][col as usize].h; // f calc
            }
        }
        
        return neigbors;
    }



    /* penso que não é mais necessário
    fn g_calc(self: &mut Self, cur_pos: (usize, usize), adj_pos: (usize, usize)) -> i32 {
        let (sides, diagonals) = self.get_neighbors_indexes(cur_pos);
        for (row, col) in sides {
            if row == adj_pos.0 && col == adj_pos.1 {
                return 10;
            }
        }
        for (row, col) in diagonals {
            if row == adj_pos.0 && col == adj_pos.1 {
                return 14;
            }
        }
        return 0;
    }
 */
    fn pathfinding(
        self: &mut Self,
        origin: (usize, usize), 
        destination: (usize, usize)
    ) {
        self.adjacency_list[origin.0][origin.1].visited = true;
        self.display_tour();
        if origin == destination {
            return;
        }
        let mut min_f_val = u32::MAX;
        let mut min_f_pos = (0, 0);
        for (row, col) in self.get_neighbors(origin, destination) {
            if self.adjacency_list[row][col].f < min_f_val {
                min_f_val = self.adjacency_list[row][col].f;
                min_f_pos = (row, col);
            }
        }
        self.tour_list[min_f_pos.0][min_f_pos.1] = true;
        self.pathfinding(min_f_pos, destination);
    }
}



fn main() {

    let mut graph = Graph::new(5, 7);
    graph.add_edge(2, 1, 8);
    graph.add_edge(2, 5, 9);
    graph.add_edge(1, 3, 1);
    graph.add_edge(2,3, 1);
    graph.add_edge(3,3, 1);
    graph.display();

    graph.pathfinding((2,1), (2,5));
}
/*
Maneira de buscar a rota mais curta de um ponto a outro sem passar por cima de locais bloqueados.

Se baseia no cálculo da distancia F

F = G + H
G: custo da origem até o ponto atual (distancia percorrida)
H: custo estimado do ponto atual até o destino
- Para calcular H se usa distancia Manhattan

   1 2 3 4 5 6 7

1  0 0 0 0 0 0 0
2  0 0 0 1 0 0 0
3  0 8 0 1 0 9 0
4  0 0 0 1 0 0 0
5  0 0 0 0 0 0 0 

Custo G:
G= 10 para lados
G= 14 para diagonais

Custo H:
H= 10*(|col_atual - col_destino| + |row_atual - row_destino|)

Lista aberta: armazena os nós a serem analisados
Lista fechada: armazena os nós já analisados

Algoritimo:
Estou em 3x2
Coloco essa posição na lista fechada.
Calculo a distancia F para todos seus vizinhos, menos posições fechadas, inválidas e barreiras
- em empate pega o primeiro
Exemplo onde o vizinho selecionado é a posição 3x3:
G = 10 pois está em uma posição não diagnonal
H = 10 * (|3-6| + |3-3|) = 30
F = 10 + 30 = 40

Verifica em todos os vizinhos qual tem o menor F
Seleciona este vizinho e o coloca na lista de fechados ... repetindo o processo

------------------------

*/
